const fs = require('fs')
const path = require('path')
const employeesDataFilePath = path.join(__dirname, './data.json')
const problem1filePath = path.join(__dirname, './problem1.json')
const problem2filePath = path.join(__dirname, './problem2.json')
const problem3filePath = path.join(__dirname, './problem3.json')
const problem4filePath = path.join(__dirname, './problem4.json')
const problem5filePath = path.join(__dirname, './problem5.json')
const problem6filePath = path.join(__dirname, './problem6.json')




/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
    { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
    
    NOTE: Do not change the name of this file
    
*/


function writeFile(filePath, data, callback) {

    fs.writeFile(filePath, JSON.stringify(data), err => {

        if (err) {
            callback(err)
        } else {
            callback(null, `${filePath}Output successfully written to json file`)
        }
    })
}


function problem1(employeesData, callback) {

    let result = employeesData.employees.filter(employee => {

        return [2, 13, 23].includes(employee.id)
    })
    writeFile(problem1filePath, { 'employees': result }, callback)
}


function problem2(employeesData, callback) {

    let result = employeesData.employees.reduce((acc, employee) => {

        if (acc[employee.company]) {
            acc[employee.company].push(employee)
        }
        return acc

    }, {
        "Scooby Doo": [],
        "Powerpuff Brigade": [],
        "X-Men": []
    })
    writeFile(problem2filePath, { 'employees': result }, callback)
}


function problem3(employeesData, callback) {

    let result = employeesData.employees.filter(employee => {

        return employee.company == 'Powerpuff Brigade'
    })
    writeFile(problem3filePath, { 'employees': result }, callback)
}


function problem4(employeesData, callback) {

    let result = employeesData.employees.filter(employee => {

        return employee.id !== 2
    })
    writeFile(problem4filePath, { 'employees': result }, callback)
}


// function problem5(employeesData, callback) {

//     let result = employeesData.employees.sort((employee1, employee2) => {

//         if (employee1.company === employee2.company) {
//             return 0
//         }

//         if (employee1.company !== employee2.company) {
//             return employee1.company - employee2.company
//         }

//         return employee1.id - employee2.id
//     })
//     writeFile(problem5filePath, { 'employees': result }, callback)
// }


// function problem6(employeesData, callback) {

//     let result = employeesData.employees

//     writeFile(problem6filePath, { 'employees': result }, callback)
// }


function employeesCallback(callback) {

    fs.readFile(employeesDataFilePath, 'utf-8', (err, data) => {

        if (err) {
            callback(err)
            return
        } else {
            console.log('Reading employees data from data.json successfully')

            employeesData = JSON.parse(data)

            problem1(employeesData, (err, data) => {
                if (err) {
                    callback(err)
                } else {
                    callback(data)

                    problem2(employeesData, (err, data) => {
                        if (err) {
                            callback(err)
                        } else {
                            callback(data)

                            problem3(employeesData, (err, data) => {
                                if (err) {
                                    callback(err)
                                } else {
                                    callback(data)

                                    problem4(employeesData, (err, data) => {
                                        if (err) {
                                            callback(err)
                                        } else {
                                            callback(data)
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}





employeesCallback((err, data) => {              // main function starts from here

    if (err) {
        console.error(err)
        return
    } else {
        console.log(data)
    }
})